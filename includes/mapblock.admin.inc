<?php
function mapblock_admin_add(){
	$mapblock = entity_create('mapblock', array());
	return drupal_get_form('mapblock_form', $mapblock);
}

function mapblock_admin_edit($mapblock){
	return drupal_get_form('mapblock_form', $mapblock);
}

/**
 * Form constructor for the product deletion confirmation form.
 *
 * @see product_delete_confirm_submit()
 */
function mapblock_delete_confirm_form ($form, &$form_state, $mapblock) {
	$form['mapblock_id'] = array('#type' => 'value', '#value' => $mapblock->id);
  return confirm_form($form,
    t('Are you sure you want to delete?', array('%title' => $mapblock->title)),
    // $_SERVER['HTTP_REFERER'],
    '/',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Executes product deletion.
 *
 */
function mapblock_delete_confirm_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $mapblock = mapblock_load($form_state['values']['mapblock_id']);
    mapblock_delete($form_state['values']['mapblock_id']);
    drupal_set_message(t('%title has been deleted.', array('%title' => $mapblock->title)));
  }
  $form_state['redirect'] = 'admin/structure/mapblock/';
}

function mapblock_form($form, $form_state, $mapblock, $op = 'add'){
	$form['mapblock'] =array(
		'#type' => 'value',
		'#value' => $mapblock
	);
	$form['title'] = array(
		'#type' => 'textfield',
		'#title' => 'Map block point name',
		'#required' => TRUE,
		'#default_value' => empty($mapblock->title) ? '' : $mapblock->title
	);
	$form['map'] = array(
		'#type' => 'fieldset',
		'#title' => t('Map info'),
		'#collapsible' => FALSE
	);
	$form['map']['coordinates'] = array(
		'#type' => 'textfield',
		'#title' => t('Coordinates'),
		'#required' => TRUE,
		'#default_value' => empty($mapblock->coordinates) ? '' : $mapblock->coordinates
	);
	$form['map']['zoom'] = array(
		'#type' => 'textfield',
		'#title' => t('Zoom'),
		'#required' => TRUE,
		'#default_value' => empty($mapblock->zoom) ? '12' : $mapblock->zoom
	);
	$form['map']['map'] = array(
		'#type' => 'markup',
		'#markup' => '<div id="map"></div>',
	);
	$lang = array('en_US' => t('English'), 'ru_RU' => t('Russian'));
	$form['map']['lang'] = array(
		'#type' => 'radios',
		'#options' => $lang,
		'#default_value' => empty($mapblock->lang) ? 'ru_Ru' : $mapblock->lang,
	);

	$settings = empty($mapblock->settings) ? array() : $mapblock->settings;

	// Settings
	$form['settings'] = array(
		'#title' => t('Map settings'),
		'#type' => 'fieldset',
		'#tree' => TRUE,
		'#collapsible' => TRUE,
		'#collapsed' => FALSE
		);
	$form['settings']['width'] = array(
		'#type' => 'textfield',
		'#title' => t('Width'),
		'#default_value' => empty($settings['width']) ? '' : $settings['width'],
		'#desctription' => t('Set map width'),
		'#size' => 10
	);
	$form['settings']['height'] = array(
		'#type' => 'textfield',
		'#title' => t('Height'),
		'#default_value' => empty($settings['height']) ? '' : $settings['height'],
		'#desctription' => t('Set map height'),
		'#size' => 10
	);
	$form['settings']['drag_toggle'] = array(
		'#type' => 'checkbox',
		'#title' => t('Toggle dragging map'),
		'#default_value' => empty($settings['drag_toggle']) ? '' : $settings['drag_toggle'],
	);
	$form['settings']['drag_toggle_label_active'] = array(
		'#type' => 'textfield',
		'#title' => t('Label active toggle'),
		'#size' => 50,
		'#default_value' => empty($settings['drag_toggle_label_active']) ? '' : $settings['drag_toggle_label_active']
	);
	$form['settings']['drag_toggle_label_passive'] = array(
		'#type' => 'textfield',
		'#title' => t('Label passive toggle'),
		'#size' => 50,
		'#default_value' => empty($settings['drag_toggle_label_passive']) ? '' : $settings['drag_toggle_label_passive']
	);

	// Options
	// Icons
	$form['settings']['options']['icon'] = array(
		'#type' => 'fieldset',
		'#title' => t('Custom icon'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#tree' => TRUE
	);
	$form['settings']['options']['icon']['path'] = array(
		'#type' => 'textfield',
		'#title' => t('Icon file path'),
		'#description' => t('Use uri wrappers eg: public://path_to_file.png'),
		'#default_value' => empty($settings['options']['icon']['path']) ? '' : $settings['options']['icon']['path']
	);
	$form['settings']['options']['icon']['size'] = array(
		'#type' => 'textfield',
		'#title' => t('Icon image size'),
		'#description' => t('Icon image width and height seperated by ",", eg: 45,35'),
		'#default_value' => empty($settings['options']['icon']['size']) ? '' : $settings['options']['icon']['size']
	);
	$form['settings']['options']['icon']['offset'] = array(
		'#type' => 'textfield',
		'#title' => t('Icon image offset'),
		'#description' => t('Icon image offset from top left seperated by ",", eg: 25,16'),
		'#default_value' => empty($settings['options']['icon']['offset']) ? '' : $settings['options']['icon']['offset']
	);
	$form['settings']['options']['icon']['cliprect'] = array(
		'#type' => 'textfield',
		'#title' => t('Icon image clip'),
		'#description' => t('If icon is part of sprite set clip rectangle coordinates coords top-left and bottom-right point as x1,y1,x2,y2 eg: 0,10,20,30'),
		'#default_value' => empty($settings['options']['icon']['cliprect']) ? '' : $settings['options']['icon']['cliprect']
	);

	// Properties
	$form['settings']['properties'] = array(
		'#type' => 'fieldset',
		'#title' => t('Properties'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#tree' => TRUE
	);
	$form['settings']['properties']['icon_content'] = array(
		'#type' => 'textfield',
		'#title' => t('Icon content'),
		'#default_value' => empty($settings['properties']['icon_content']) ? '' : $settings['properties']['icon_content'],
	);
	$form['settings']['properties']['hint_content'] = array(
		'#type' => 'textfield',
		'#title' => t('Hint content'),
		'#default_value' => empty($settings['properties']['hint_content']) ? '' : $settings['properties']['hint_content'],
	);
	$form['settings']['properties']['balloon_content_header'] = array(
		'#type' => 'text_format',
		'#title' => t('Balloon content header'),
		'#default_value' => empty($settings['properties']['balloon_content_header']['value']) ? '' : $settings['properties']['balloon_content_header']['value']
	);
	$form['settings']['properties']['balloon_content_body'] = array(
		'#type' => 'text_format',
		'#title' => t('Balloon content body'),
		'#default_value' => empty($settings['properties']['balloon_content_body']['value']) ? '' : $settings['properties']['balloon_content_body']['value']
	);
	$form['settings']['properties']['balloon_content_footer'] = array(
		'#type' => 'text_format',
		'#title' => t('Balloon content footer'),
		'#default_value' => empty($settings['properties']['balloon_content_footer']['value']) ? '' : $settings['properties']['balloon_content_footer']['value']
	);

	$module_path = drupal_get_path('module', 'mapblock');
	$form['#attached']['js'][] = array(
		'data' => 'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
		'type' => 'external'
	);
	$form['#attached']['js'][] = $module_path . '/js/mapblock.admin.js';
	$form['#attached']['css'][] = $module_path . '/css/mapblock.admin.css';

	$form['actions'] = array('#type' => 'actions');

	$form['actions']['submit'] = array(
		'#value' => t('Save'),
		'#type' => 'submit'
	);
	return $form;
}

function mapblock_form_submit(&$form, &$form_state){
	form_state_values_clean($form_state);
	$values = $form_state['values'];
	$mapblock = $values['mapblock'];
	$mapblock->title = $values['title'];
	$mapblock->coordinates = $values['coordinates'];
	$mapblock->zoom = $values['zoom'];
	$mapblock->lang = $values['lang'];
	$mapblock->settings = $values['settings'];
	$mapblock->save();
	$form_state['redirect'] = 'admin/structure/mapblock';
}

function mapblock_view_mapblocks(){
	$mapblocks = mapblock_load_multiple();
	$rows = array();
	$header = array('title', 'actions');
	foreach($mapblocks as $mapblock){
		$rows[] = array(
			$mapblock->title,
			l(t('Edit'), 'admin/structure/mapblock/' . $mapblock->id . '/edit') . ' ' .
			l(t('Delete'), 'admin/structure/mapblock/' . $mapblock->id . '/delete') 
		);
	}
	$out['links'] = array(
		'#theme' => 'item_list',
		'#items' => array(
			l(t('Add map block'), 'admin/structure/mapblock/add')
			),
		'#type' => 'ul'
	);
	$out['table'] =  array(
		'#theme' => 'table',
		'#header' => $header,
		'#rows' => $rows
	);
	return $out;
}