(function($){
	Drupal.behaviors.mapblock = {
		attach: function(context, settings){
			var mapblocks = settings.mapblocks;
			var myMap = {}, myPlacemark = {};
			if($('.mapblock').once().length){
				function init(){
					for(var i in mapblocks){
						if(mapblocks.hasOwnProperty(i)){
							var id = mapblocks[i].id;
							var center = [
								parseFloat(mapblocks[i].coordX),
								parseFloat(mapblocks[i].coordY),
							];
							var zoom = mapblocks[i].zoom;

							myMap[id] = new ymaps.Map('map-' + id, {
								center: center,
								zoom: zoom,
								behaviors: ["drag", "dblClickZoom", "rightMouseButtonMagnifier", "multiTouch"],
								controls: ['zoomControl','searchControl','typeSelector'] }, {
								searchControlProvider: 'yandex#search'
							});
							var  placemark_properties = {};
							var properties = mapblocks[i].settings.properties;
							if (properties) {
								placemark_properties.iconContent = properties.icon_content;
								placemark_properties.hintContent = properties.hint_content;
								placemark_properties.balloonContentHeader = properties.balloon_content_header.value;
								placemark_properties.balloonContentBody = properties.balloon_content_body.value;
								placemark_properties.balloonContentFooter = properties.balloon_content_footer.value;
							}
							var placemark_options = {};
							var icon = mapblocks[i].settings.options.icon;
							var tmpArr;
							if (icon.path && icon.size) {
								placemark_options.iconLayout = 'default#image';
								placemark_options.iconImageHref = icon.path;
								tmpArr = icon.size.split(',', 2);
								placemark_options.iconImageSize = [parseInt(tmpArr[0]), parseInt(tmpArr[1])];
								tmpArr = icon.offset.split(',', 2);
								placemark_options.iconImageOffset = [parseInt(tmpArr[0]), parseInt(tmpArr[1])];
								if (icon.cliprect) {
									tmpArr = icon.cliprect.split(',', 4);
									placemark_options.iconImageClipRect = [[parseInt(tmpArr[0]), parseInt(tmpArr[1])], [parseInt(tmpArr[2]), parseInt(tmpArr[3])]];
								}
							}
							myPlacemark[id] = new ymaps.Placemark(center, placemark_properties, placemark_options);

							if (mapblocks[i].settings['drag_toggle']) {
								myMap[id].behaviors.disable("drag");
								var $mapDragToggle = $('<div id="map-drag-toggle-' + i + '" class="map-mobile-toggle"><a href="#">' + mapblocks[i].settings['drag_toggle_label_passive'] + '</a></div>')
									.appendTo($('#map-' + id));
								$mapDragToggle.click(function (e) {
									var $link = $('a', $mapDragToggle);
									var behaviors = myMap[id].behaviors;
									var $this = $(this);
									if (behaviors.isEnabled('drag')) {
										behaviors.disable('drag');
										$this.removeClass('draggable');
										$link.text(mapblocks[i].settings['drag_toggle_label_passive']);
									} else {
										behaviors.enable('drag');
										$this.addClass('draggable');
										$link.text(mapblocks[i].settings['drag_toggle_label_active']);
									}
									e.preventDefault();
								});
							}

							myMap[id].geoObjects.add(myPlacemark[id]);
							}
						}

				}
				ymaps.ready(init);

			}
		}
	};
})(jQuery);
