(function($){
	var myMap;

	ymaps.ready(init);
	function init() {
		function createPlacemark(coords) {
			return new ymaps.Placemark(coords, {
			}, {
				preset: 'islands#violetStretchyIcon',
				draggable: true
			});
		}
		function saveCoords(coords) {
			$coordsField.val(coords);
		}
		var myMap;
		var myPlacemark;
		var $coordsField = $('input[name="coordinates"]');
		var tmpCoords = $coordsField.val().split(',');
		if (tmpCoords.length == 2) {
			var existCoords = [
			parseFloat(tmpCoords[0]),
			parseFloat(tmpCoords[1])
			];
		} else {
			var existCoords = [];
		}
		if (existCoords.length) {
			var center = existCoords;
		} else {
			var center = [45.048796,38.978914]; //Краснодар
		}
		var zoom = $('input[name="zoom"]').val();
		zoom = !zoom ? 12 : zoom;
		myMap = new ymaps.Map('map', {
			center: center,
			zoom: zoom,
			controls: ['zoomControl','searchControl','typeSelector']
		}, {
			searchControlProvider: 'yandex#search'
		});
		if (existCoords.length) {
			myPlacemark = createPlacemark(center);
			myMap.geoObjects.add(myPlacemark);
			myPlacemark.events.add('dragend', function () {
				saveCoords(myPlacemark.geometry.getCoordinates());
			});
		}
		myMap.events.add('click', function (e) {
			var coords = e.get('coords');
			saveCoords(coords);
			if (myPlacemark) {
				myPlacemark.geometry.setCoordinates(coords);
			}
	        // Если нет – создаем.
	        else {
	        	myPlacemark = createPlacemark(coords);
	        	myMap.geoObjects.add(myPlacemark);
	          // Слушаем событие окончания перетаскивания на метке.
	          myPlacemark.events.add('dragend', function () {
	          	saveCoords(myPlacemark.geometry.getCoordinates());
	          });
	      }
	  });
		hideToggleLabel();
		$('#edit-settings-drag-toggle').click(hideToggleLabel);
		
		function hideToggleLabel(e) {
			var $toggleLabelActive = $('.form-item-settings-drag-toggle-label-active');
			var $toggleLabelPassive = $('.form-item-settings-drag-toggle-label-passive');
			var $toggleDragging = $('#edit-settings-drag-toggle');
			if ($toggleDragging.prop('checked')) {
				$toggleLabelActive.show();
				$toggleLabelPassive.show();
			} else {
				$toggleLabelActive.hide();
				$toggleLabelPassive.hide();
			}
		}	
	}

})(jQuery);